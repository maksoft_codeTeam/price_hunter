from price.models import Link
from price.pusher import Pusher
from price.spiders.ebag.product import Product


def run():
    links = Link.objects.filter(checked=False, valid=True)
    for link in links:
        try:
            print(link)
            p = Product(link)
            p.populate()
            print(p.product)
            push = Pusher(p.product)
            push.push()
            link.checked = True
            link.valid = True
            link.save()
            print('successfull uploaded product')
        except Exception as e:
            link.checked = True
            link.valid = False
            link.save()
            print(e)
