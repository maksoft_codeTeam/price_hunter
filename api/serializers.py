from price.models import Product, Link, Store
from rest_framework import serializers


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ('id', 'name', 'domain', 'updated_at')


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ('id', 'url', 'domain', 'key', 'checked', 'valid')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'code', 'name', 'group', 'brand', 'price', 'old_price',
                  'stock', 'guarantee', 'content', 'discount', 'link',
                  'visited_at', 'updated_at')
