from rest_framework import viewsets, mixins
from api.serializers import LinkSerializer, ProductSerializer, StoreSerializer
from price.models import Link, Product, Store


class StoreViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Store.objects.all()

    serializer_class = StoreSerializer


class LinkViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    queryset = Link.objects.all()
    serializer_class = LinkSerializer
    lookup_field = 'key'

    def get_queryset(self):
        if self.lookup_field in self.kwargs:
            return Link.objects.filter(key=self.kwargs[self.lookup_field])
        return Link.objects.all()


class ProductViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = Product.objects.all()

    serializer_class = ProductSerializer
