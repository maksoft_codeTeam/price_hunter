from django.shortcuts import render
from price.models import Store, Link, Product
import time
from datetime import datetime
from urllib.parse import urljoin
from django.http import HttpRequest, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
import requests
import tldextract
from bs4 import BeautifulSoup
from decimal import *
from price.pusher import Pusher
from price.spiders.ebag.product import Product
from price.spiders.helpers import *


# Create your views here.

import tldextract
from urllib.parse import urlparse


def save(session, obj):
    session.add(obj)
    session.commit()


def crawl_links(url, store):
    response = ''
    count = 0
    a = []
    try:
        r = requests.get(url, timeout=5)
        soup = BeautifulSoup(r.text, 'html.parser')

        domain_on_redirect = get_domain(r.url)
        link = Link.objects.filter(url=domain_on_redirect, domain=store).first()

        for site_url in set([o.get('href') for o in soup.find_all('a')]):
            if site_url is None:
                continue

            response += 'Found link: {}<br>'.format(site_url)
            url = site_url
            if not is_url(site_url):
                url = urljoin(get_domain(store.domain), site_url)

            if get_domain(store.domain) == get_domain(url):
                l = Link.objects.filter(url=url, domain=store).first()

                if l is None:
                    l = Link(url=url, domain=store)
                    l.save()
                    response += 'save()<br>'
                    count += 1
                    print(url)
                    response += crawl_links(url, store)

    except Exception as e:
        print('something went wrong <br> {}'.format(e))

    return "{} <br> Total saved links for domain {} : <br><strong>{}"\
            .format(response, store.domain, count)


def crawl(request):
    store = Store.objects.filter(updated_at=None).first()
    a = []
    a.append(crawl_links(store.domain, store))
    store.updated_at = datetime.now()
    return HttpResponse('<br>'.join(map(str, a)))


def getProducts(request):
    links = Link.objects.filter(checked=False, valid=True)
    m = ''
    for link in links:
        try:
            print(link)
            p = Product(link)
            p.populate()
            print(p.product)
            push = Pusher(p.product)
            push.push()
            link.checked = True
            link.valid = True
            link.save()
            m = 'successfull uploaded product'
            print(m)
        except Exception as e:
            m = e
            link.checked = True
            link.valid = False
            link.save()
    return HttpResponse(m)


def priceValidator(price):
    if type(price) is list:
        price = price[0]
    if not price:
        return 0
    price = price.replace(" лв.", "")
    price = price.replace(",", ".")
    return price.strip()
