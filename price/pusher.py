# pusher.py
import requests
from price.spiders.settings import API_AUTH_TOKEN


class Pusher:
    interface = {
            'product': {"url": "http://qms-manager.com/rest/products/"},
            'stpre': {},
            'link': {}
           }
    auth_uri = "http://qms-manager.com/api-auth/login/?next=/rest/"

    def __init__(self, post_data):
        self.data = post_data
        self.headers = {"Authorization": "Token {}".format(API_AUTH_TOKEN)}
        pass

    def __str__(self):
        return self.__name__

    def getProduct(self, id):
        return "http://qms-manager.com/rest/products/{}/".format(id)

    def auth(self):
        with requests.Session() as c:
            redirect = c.get(self.getProduct(3), headers=self.headers)
            return redirect.content

    def push(self):
        with requests.Session() as c:
            p = c.post("http://qms-manager.com/rest/products/",
                       data=self.data,
                       headers=self.headers)
            if p.status_code >= 300:
                raise Exception(p.text)
            return p.status_code

    def save(self, command):
        if command not in self.interface:
            raise Exception("Invalid command")
        p = self.push(self.interface[command]['url'])
        print(p.content)
