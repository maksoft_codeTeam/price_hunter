# eba_product_scrapper.py
import requests
from bs4 import BeautifulSoup
from decimal import Decimal
from price.decorators import is_null, sanitaze_string


class Product:
    _stock = True
    _guarantee = 0
    product = {}

    def __init__(self, link):
        self.link = link
        r = requests.get(self.link.url, timeout=5)
        self.soup = BeautifulSoup(r.content, 'lxml')

    def __str__(self):
        return 'Product'

    def __repr__(self):
        return 'Product'

    def __len__(self):
        return len(self.product)

    def __getitem__(self, key):
        if key in self.product:
            return self.product[key]
        return None

    def __setitem__(self, key, value):
        self.product[key] = value

    def __delitem__(self, key):
        del self.product[key]

    def __iter__(self):
        return iter(self.product)

    def append(self, value):
        self.product.append(value)

    def head(self):
        # get the first element
        return self.product[0]

    def tail(self):
        # get all elements after the first
        return self.product[1:]

    def init(self):
        # get elements up to the last
        return self.product[:-1]

    def last(self):
        # get last element
        return self.product[-1]

    def drop(self, n):
        # get all elements except first n
        return self.product[n:]

    def take(self, n):
        # get first n elements
        return self.product[:n]

    def populate(self):
        content = self.soup.findAll('div', {"class": "content"})
        for el in content:
            self.product['code'] = self._getCode(el)
            self.product["name"] = self._getName(el)
            new_price = Decimal(self._getActualPrice(el))
            old_price = Decimal(self._getOldPrice(el))
            d = self._getDiscount(new_price, old_price)
            self.product["price"] = str(new_price)
            self.product["old_price"] = str(old_price)
            self.product['discount'] = str(d)
            self.product["group"] = self._getGroup()
            self.product["stock"] = self._stock
            self.product["guarantee"] = self._guarantee
            self.product["content"] = self._getContent()
            self.product["link"] = self.link.id
            return self.product

    @is_null
    def _getName(self, el):
        name = [name.text for name
                in el.findChildren("h2", {"class": "product-title"})]
        name = "".join(name)
        return name

    @sanitaze_string
    def _getCode(self, el):
        attr = "data-code"
        code = [item.text
                for item in el.findChildren(
                    'strong', {attr: ""}) if attr in item.attrs]
        return code

    @sanitaze_string
    @is_null
    def _getActualPrice(self, el):
        attr = "data-var-price"
        price = [y.text
                 for x in el.findChildren(
                    'div', {'class': 'price'}) for y in x if attr in y.attrs]
        return price

    @sanitaze_string
    def _getOldPrice(self, el):
        attr = "data-old-price"
        price = [y.text for x in el.findChildren('div', {'class': 'price'}) for
                 y in x if attr in y.attrs]
        if price == 0:
            return 0
        return price

    def _getDiscount(self, act_price, old_price):
        perc = ((old_price - act_price) / ((old_price + act_price)/2)) * 100
        return perc.quantize(0)

    def _getGroup(self):
        category = self.soup.findChildren("div", {"class": "wrapper"})
        breadcrumb = ''
        for el in category:
            if el.findAll("ul", {"class": "breadcrumb"}):
                breadcrumb = el.find("ul", {"class": "breadcrumb"})

                if len(breadcrumb) > 7:
                    li = [li for li in breadcrumb]
                    return li[(len(li)-6)].text

    def _getContent(self):
        return self.soup.find("div", {"class": "tiny-mce"}).text
