import requests
import json
import hashlib
from urllib.parse import urljoin
from settings import API_AUTH_TOKEN


class Link:

    data = {}
    end_point = "http://qms-manager.com/rest/links/"

    headers = {"Authorization": "Token {}".format(API_AUTH_TOKEN)}

    def __init__(self, url, domain):
        self.data['url'] = url
        self.data['domain'] = domain
        self.data['checked'] = False
        self.data['valid'] = True

    def __str__(self):
        return self.data['url']

    def load(self):
        try:
            self.headers["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_5)\
 AppleWebKit/536.32 (KHTML,like Gecko) Chrome/53.0.2717.78 Safari/536.32"
            needle = hashlib.sha1(self.data['url'].encode())
            needle = needle.hexdigest()
            url = urljoin(self.end_point, needle)
            r = requests.get(url, headers=self.headers)
            return True
        except Exception as e:
            return False

    def save(self):
        try:
            r = requests.post(self.end_point, data=self.data,
                              headers=self.headers)
            return self.check_status(r)
        except Exception as e:
            return e

    def check_status(self, r):
        if r.status_code >= 220:
            raise Exception(r.text)
        data = json.loads(r.text)
        for j in data:
            self.data[j] = data[j]
        return r.status_code

    def __getattr__(self, key):
        print(key)
        if key in self.data:
            return self.data[key]
        return None

    def __setattr__(self, key, value):
        self.data[key] = value

    def __delattr__(self, key):
        del self.data[key]

    def __iter__(self):
        return iter(self.data)
