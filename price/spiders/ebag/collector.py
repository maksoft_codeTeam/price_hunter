import requests
from time import sleep
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from helpers import get_domain, is_url
from ebag.link import Link


def ebag_links(*args, **kwargs):
    category = 1
    offset = 12
    store_url = 'http://www.ebag.bg/'
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_5)\
 AppleWebKit/536.32 (KHTML,like Gecko) Chrome/53.0.2717.78 Safari/536.32"}
    while True:
        links = 0
        url = "http://www.ebag.bg/axProductsList.php?maxPrice=\
99999&minPrice=0&orderField=&order=&Id={}&offset={}".format(category, offset)
        print(url)
        try:
            r = requests.get(url, headers=headers,
                             allow_redirects=True, timeout=5)
            soup = BeautifulSoup(r.text, 'lxml')
            domain_on_redirect = get_domain(r.url)

            l = Link(domain_on_redirect, 1)

            if len(r.text) < 100:
                raise Exception("empty page")

            l.save()
            for site_url in set([o.get('href') for o in soup.find_all('a')]):
                print(site_url)
                if site_url is None:
                    continue

                print("Found link: {}".format(site_url))

                if not is_url(site_url):
                    site_url = urljoin(get_domain(store_url), site_url)

                print(get_domain(url))
                print(site_url)
                if get_domain(store_url) == get_domain(url):
                    l = Link(site_url, 1)
                    l.save()
                    links += 1

            if links == 0:
                raise Exception('no links')

        except Exception as e:
            print('{}'.format(e))
            category += 1
            offset = 12

        offset += 12
        print('sleep for 2 seconds')
        sleep(2)
