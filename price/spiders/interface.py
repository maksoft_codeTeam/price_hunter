import sys
from prettytable import PrettyTable as pt
from ebag.link import Link
from ebag.collector import ebag_links


class Interface:

    quit = False
    spiders = {}
    products = {}

    def __init__(self):
        pass

    def add(self, crawler, name):
        self.spiders[name] = crawler

    def products(self, product, name):
        self.products[name] = product

    def list_spiders(self, *args, **kwargs):
        return "Total: \n".join([crawler for crawler in self.spiders.keys()])

    def help(self, *args):
        table = pt(['Command', 'Info'])
        table.add_row(["list_spiders", 'list of attached spiders'])
        table.add_row(["crawl", "crawl <spider name>"])
        table.add_row(["product", "get product <spider name>"])
        table.add_row(["exit", ""])
        return table

    def crawl(self, *args, **kwargs):
        if args[0]:
            args = "".join(args[0])
        if args in self.spiders:
            return self.spiders[args]()
        return "Invalid Spider name. Type list_spiders for list\
with add added spiders"

    def product(self, *args, **kwargs):
        if args[0]:
            args = "".join(args[0])
        if args in self.products:
            return self.product[args]()
        return "Invalid Spider name. Type list_spiders for list\
with add added spiders"

    def cmd(self, *args):
        cmd = {
            "help": self.help,
            "list_spiders": self.list_spiders,
            "crawl": self.crawl,
            "product": self.product,
            "exit": self.exit
            }
        if args[0] in cmd:
            return cmd[args[0]]
        return self.invalid_syntax()

    def main(self):
        welcome = open('welcome.txt', 'r')
        while self.quit is False:
            user_input = input(welcome.read()).split()
            try:
                print(self.cmd(user_input[0])(user_input[1:]))
            except Exception as e:
                print(e)
                print(self.invalid_syntax())

    def invalid_syntax(self, *args, **kwargs):
        return "Invalid syntax please type help for more info"

    def exit(self, *args, **kwargs):
        self.quit = True
        return "Goodbye!"


def main():
    i = Interface()
    i.add(ebag_links, "ebag.bg")
    i.main()


if __name__ == '__main__':
    main()
