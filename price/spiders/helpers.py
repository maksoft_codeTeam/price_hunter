import tldextract
from urllib.parse import urlparse


def is_url(url):
    return tldextract.extract(url).registered_domain != ''


def get_domain(url):
    url_parts = urlparse(url)
    scheme = url_parts.scheme

    if scheme == '':
        scheme = 'http'

    domain = tldextract.extract(url).registered_domain

    return "{}://{}".format(scheme, domain)


def url_exists(Link, href):
    link = Link.objects.filter(url=href).first()
    return link is not None
