from django.db import models
import hashlib


class Store(models.Model):
    name = models.CharField(max_length=128)
    domain = models.URLField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.domain


class Link(models.Model):
    url = models.CharField(max_length=255, unique=True)
    domain = models.ForeignKey(Store)
    key = models.CharField(max_length=56, null=True, blank=True)
    visited_at = models.DateField(auto_now=True)
    checked = models.BooleanField(default=False)
    valid = models.BooleanField(default=True)

    def __str__(self):
        return self.url

    def save(self, *args, **kw):
        sha1 = hashlib.sha1(self.url.encode())
        self.key = sha1.hexdigest()
        super(Link, self).save(*args, **kw)


class Product(models.Model):
    code = models.CharField(max_length=168)
    name = models.CharField(max_length=255)
    group = models.CharField(max_length=178, blank=True, null=True)
    brand = models.CharField(max_length=178, blank=True, null=True)
    price = models.CharField(max_length=32, blank=False, null=False, default=0)
    old_price = models.CharField(max_length=32,
                                 blank=True,
                                 null=True,
                                 default=0)
    stock = models.BooleanField(default=True)
    guarantee = models.CharField(max_length=4, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    discount = models.IntegerField(blank=True, null=True, default=0)
    link = models.ForeignKey(Link)
    visited_at = models.DateField(auto_now=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return "{}[{}] лв.".format(self.name, self.code)
