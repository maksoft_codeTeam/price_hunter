from django.contrib import admin
from price.models import Store, Link, Product


class ProductAdmin(admin.ModelAdmin):
    model = Product
    date_hierarchy = 'updated_at'

    list_display = ('name', 'code', 'price',
                    'old_price', 'discount',
                    'visited_at', 'updated_at')

    list_filter = (('stock', admin.BooleanFieldListFilter),
                   'visited_at', 'updated_at')

    search_fields = ['code', 'name', 'content']


class LinkAdmin(admin.ModelAdmin):
    model = Link
    list_display = ('url', 'domain', 'valid', 'checked')
    list_filter = (('checked', admin.BooleanFieldListFilter),
                   ('valid', admin.BooleanFieldListFilter)
                   )
    search_fields = ['url', 'key']


class StoreAdmin(admin.ModelAdmin):
    model = Store
    list_display = ('name', 'domain', 'created_at', 'updated_at')
    list_filter = ('created_at', 'updated_at')
    search_fields = ['name', 'domain']


admin.site.register(Store, StoreAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(Product, ProductAdmin)
