from functools import wraps


def sanitaze_string(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        res = f(*args, **kwargs)
        if type(res) is list:
            res = "".join(res)
        if not res:
            return 0
        res = res.replace(" лв.", "")
        res = res.replace(",", ".")
        return res.strip()
    return wrapper


def is_null(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        res = f(*args, **kwargs)
        if res:
            return res
        raise Exception("Invalid data")
    return wrapper


def debug(func):
    msg = func.__qualname__

    @wraps(func)
    def wrapper(*args, **kwargs):
        print(msg)
        r = func(*args, **kwargs)
        print(r)
        return r
    return wrapper
